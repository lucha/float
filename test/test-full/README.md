"Full-featured" (more or less) test environment, including the main
infrastructure services, running on three Vagrant nodes: one front-end,
two back-ends. Includes a *real* service, a trivial HTTP server that
replies "hello" to all requests (git.autistici.org/ai3/docker-okserver).

