Minimal test environment that runs a trivial containerized service.

The test service
is [docker-okserver](https://git.autistici.org/ai3/docker-okserver),
an HTTP server that always returns the string "OK".

The test will verify that the service is exported via the public HTTP
router as *ok.example.com*.
