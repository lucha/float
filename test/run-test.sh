#!/bin/bash

# Find the absolute path to this script's directory
# (so that we can find the 'float' root dir).
bin_dir=$(dirname "$0")
bin_dir=${bin_dir:-.}
bin_dir=$(cd "${bin_dir}" && pwd)
float_dir="${bin_dir}/.."

die() {
    echo "ERROR: $*" >&2
    exit 1
}

clean() {
    rm -f group_vars/all/secrets.yml
    rm -f group_vars/all/apt-proxy.yml
    rm -f .ansible_vault_pw
    rm -f ansible.log
    rm -fr conf
    stop_vagrant
}

start_vagrant() {
    vagrant up \
        || die "could not start virtual machines"
}

stop_vagrant() {
    vagrant destroy -f
}

check_hosts_ready() {
    # Wait at most 30 seconds for the vms to become reachable.
    local i=0
    while [ $i -lt 10 ]; do
        sleep 3
        ANSIBLE_VAULT_PASSWORD_FILE=.ansible_vault_pw \
            ansible -i config.yml all -m ping \
            && break
        i=$(($i + 1))
    done
    [ $i -eq 10 ] && die "could not reach virtual machines over SSH"
}

setup_ansible_env() {
    echo -n "justarandompassword" > .ansible_vault_pw
    rm -f group_vars/all/secrets.yml || true

    if [ -n "${apt_proxy}" ]; then
cat > group_vars/all/apt-proxy.yml <<EOF
---
apt_proxy: ${apt_proxy}
apt_proxy_enable_https: true
EOF
    fi

    # Install a test dhparam file to save some time.
    mkdir -p conf/x509
    cp ${bin_dir}/dhparam.test conf/x509/dhparam
}

run_ansible() {
    ANSIBLE_VAULT_PASSWORD_FILE="${PWD}/.ansible_vault_pw" \
        ../../run-playbook -i config.yml ${float_dir}/playbooks/init-credentials.yml \
        || die "failed to run the init-credentials.yml playbook"
    ANSIBLE_VAULT_PASSWORD_FILE="${PWD}/.ansible_vault_pw" \
        ../../run-playbook -i config.yml site.yml \
        || die "failed to run the site.yml playbook"
}

usage() {
    cat <<EOF
Usage: $0 [<options>] <test-dir>

Executes a Vagrant-based test of the Ansible configuration
contained in 'test-dir'.

Known options:
  --keep            Do not destroy the VMs after running tests.
  --apt-proxy ADDR  Use the specified APT proxy (host:port).
  --help            Print this help message.

EOF
}

keep_vms=0
apt_proxy=
while [ $# -gt 0 ]; do
    case "$1" in
        --keep)
            keep_vms=1
            ;;
        --apt-proxy)
            shift
            apt_proxy="$1"
            ;;
        --help|-h)
            usage
            exit 0
            ;;
        -*)
            echo "Unknown option '$1'" >&2
            usage >&2
            exit 2
            ;;
        *)
            break
            ;;
    esac
    shift
done

test_name="$1"
if [ -z "${test_name}" ]; then
    usage >&2
    exit 2
fi
if [ ! -d "${test_name}" ]; then
    echo "Test directory ${test_name} not found" >&2
    exit 1
fi

cd ${test_name}
clean
start_vagrant
setup_ansible_env
check_hosts_ready
run_ansible

# Execute the env-specific tests, if any.
if [ -e test.sh ]; then
    sh test.sh \
        || die "env-specific test suite failed"
fi

if [ ${keep_vms} -eq 0 ]; then
    stop_vagrant
fi

exit 0
