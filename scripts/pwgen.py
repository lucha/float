#!/usr/bin/env python
# pwgen.py
# Creates and provides passwords to Ansible.
#
# See also ansible sources: lib/ansible/parsing/yaml/constructor.py

import base64
import sys
import optparse
import os
import random
import shutil
import subprocess
import string
import tempfile
import yaml


def decrypt(src):
    return subprocess.check_output(
        ['ansible-vault', 'decrypt', '--output=-', src])


def encrypt(data, dst):
    p = subprocess.Popen(
        ['ansible-vault', 'encrypt', '--output=' + dst, '-'],
        stdin=subprocess.PIPE)
    p.communicate(data)
    rc = p.wait()
    if rc != 0:
        raise Exception('ansible-vault encrypt error')


def generate_simple_password(length=32):
    """Simple password generator.

    The resulting passwords should be alphanumeric, easily
    cut&pastable and usable on the command line.
    """
    n = length * 5 / 8
    return base64.b32encode(os.urandom(n)).rstrip('=')


def generate_binary_secret(length=32):
    """Binary password generator.

    Generates more complex passwords. Unfortunately at the moment the
    result needs to be UTF8-encodable due to Ansible limitations, so
    we can't just grab some random bytes from os.urandom() and we
    base64-encode them instead.

    """
    n = length * 3 / 4
    return base64.b64encode(os.urandom(n)).rstrip('=')


def generate_password(entry):
    ptype = entry.get('type', 'simple')
    if ptype == 'simple':
        return generate_simple_password(length=entry.get('length', 32))
    elif ptype == 'binary':
        return generate_binary_secret(length=entry.get('length', 32))
    else:
        raise Exception('Unknown password type "%s"' % ptype)


def main():
    parser = optparse.OptionParser(usage='%prog <password file>')
    parser.add_option('--vars', metavar='FILE', dest='vars',
                      default='vars/passwords',
                      help='Output vars file')
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Not enough arguments')

    if not os.getenv('ANSIBLE_VAULT_PASSWORD_FILE'):
        print >>sys.stderr, "You need to set ANSIBLE_VAULT_PASSWORD_FILE"
        sys.exit(2)

    password_file = args[0]
    vars_file = opts.vars
    passwords = {}

    if os.path.exists(vars_file):
        passwords.update(yaml.load(decrypt(vars_file)))

    changed = False
    with open(password_file) as fd:
        for entry in yaml.load(fd):
            name = entry['name']

            if name not in passwords:
                print >>sys.stderr, "Generating password for '%s'" % name
                passwords[name] = generate_password(entry)
                changed = True

    if changed:
        encrypt(yaml.dump(passwords), vars_file)


if __name__ == '__main__':
    main()
