ldap
====

This role installs a master/slave OpenLDAP setup, with *syncrepl*
replication.

The database directory is mounted in memory on the slaves, for
performance reasons (our production dataset is < 1G). The size of the
tmpfs is controllable with the *ldap_tmpfs_size* configuration
variable.

The service does not run (yet?) in a container because I don't know
how to make it listen on a public UNIX socket, but there is some
sandboxing provided by systemd.

Manager credentials are never stored on the servers. Other services
can define LDAP users, if they need access to the data, by defining a
*ldap_credentials* attribute, a list of dictionaries, currently whose
only supported attribute is *name*. Corresponding LDAP objects will be
created under *ou=Operators*. Passwords for these accounts must be
manually added to the *passwords.yml* file, under the name
`ldap_NAME_password`. Currently, associated ACLs must be added
manually to the LDAP configuration.

Anonymous access to the LDAP database is disallowed, except for
connections coming through the local UNIX socket (for performance
reasons). Access to the socket is controlled via membership to the
*openldap* UNIX group.
