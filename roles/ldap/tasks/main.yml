---

# The defaults file will stop slapd from ever starting, even on the
# first install.
- copy:
    content: "SLAPD_NO_START=1"
    dest: "/etc/default/slapd"

# We used to do this in the ldap slack role, but it seems they make
# things worse and cause slapd to attempt an initial installation.
#- name: Pre-seed the slapd package to skip configuration
#  shell: "echo slapd slapd/no_configuration boolean true | debconf-set-selections"
#- file: path=/etc/ldap/slapd.d state=directory

# On a new machine we have to install the package *before* touching
# anything in /etc/ldap, or it will think there is a previous
# installation and start backing it up (failing).
- name: Install the LDAP packages
  apt: name={{ item }} state=latest
  with_items:
    - slapd
    - ldap-utils

# If we install slapd this way, it won't even create the openldap
# user, so we have to do it ourselves.
- name: Create the openldap group
  group: name=openldap system=yes
- name: Create the openldap user
  user: name=openldap group=openldap home=/var/lib/ldap createhome=no system=yes 

# Install the LDAP configuration
- name: Install LDAP configuration (dirs)
  file:
    path: "/etc/ldap/{{ item.path }}"
    state: directory
  with_filetree: config/
  when: item.state == 'directory'
- name: Install LDAP configuration (schemas)
  copy:
    src: "{{ item.src }}"
    dest: "/etc/ldap/{{ item.path }}"
  with_filetree: config/
  when: item.state == 'file'
  notify:
    - reload slapd

- name: Assign server ID
  set_fact: ldap_server_id={{item.0 + 1}}
  with_indexed_items: "{{groups['ldap']}}"
  when: "item.1 == inventory_hostname"

- name: Install LDAP configuration (slapd.conf)
  template:
    src: "slapd.conf.j2"
    dest: "/etc/ldap/slapd.conf"
  notify:
    - reload slapd

- name: Create /var/lib/ldap
  file: path=/var/lib/ldap owner=openldap group=openldap mode=0700 state=directory

- name: Create /var/lib/ldap-accesslog
  file: path=/var/lib/ldap-accesslog owner=openldap group=openldap mode=0700 state=directory
  when: ldap_master

# Mount /var/lib/ldap as a tmpfs on slaves (setup fstab).
- name: Setup /var/lib/ldap tmpfs
  lineinfile:
    path: "/etc/fstab"
    line: "none /var/lib/ldap tmpfs rw,uid=openldap,mode=0700,size={{ ldap_tmpfs_size }} 0 0"
  when: "not ldap_master"

# The opposite of the above: on the master, ensure /var/lib/ldap is
# *not* mounted as tmpfs, and actually unmount it if it is.
- name: Clearing tmpfs mount for /var/lib/ldap
  lineinfile:
    path: "/etc/fstab"
    regexp: "^none /var/lib/ldap"
    state: absent
  when: ldap_master

# Actually mount /var/lib/ldap if it's not mounted (like on the first
# execution, it will be mounted at the next reboot).
- name: Mount /var/lib/ldap
  shell: "grep -q '^none /var/lib/ldap' </proc/mounts || mount /var/lib/ldap"
  when: "not ldap_master"

- name: Umount /var/lib/ldap
  shell: "grep -q '^none /var/lib/ldap' </proc/mounts && umount /var/lib/ldap || true"
  when: ldap_master

- name: Install our slapd systemd service
  template:
    src: "slapd.service.j2"
    dest: "/etc/systemd/system/slapd.service"
  register: slapd_service

- name: Update systemd
  systemd: name=slapd.service state=started masked=no enabled=yes daemon_reload=yes
  when: "slapd_service|changed"

# Bootstrap the data when we are the master and data isn't loaded yet.
# We use a flag file for this, which works for testing, but in reality
# we should handle the slave -> master transition a bit better (with a
# dump and reload of the database).
- name: Check bootstrap
  stat:
    path: /etc/ldap/.bootstrap_ok
  when: ldap_master
  register: ldap_bootstrap_ok

- include_tasks: bootstrap.yml
  when: "ldap_master and not ldap_bootstrap_ok.stat.exists"
