---

# SSO proxy setup.
- name: Install sso-proxy
  apt: name=sso-proxy state=latest

- name: Configure /etc/default/sso-proxy
  copy: src=sso-proxy.default dest=/etc/default/sso-proxy
  notify: restart sso-proxy

- name: Configure ssoproxy
  template:
    src: proxy.yml.j2
    dest: /etc/sso/proxy.yml
    owner: root
    group: sso-proxy
    mode: 0640
  notify: restart sso-proxy

- name: Add user sso-proxy to credentials group
  user: name=sso-proxy groups=ssoproxy-credentials append=yes

- name: Enable sso-proxy systemd unit
  systemd: name=sso-proxy.service enabled=yes masked=no

# NGINX setup.
- name: Install NGINX
  apt: name=nginx-full state=latest

# Create the nginx user, grant it membership to the group that owns the credentials.
- name: Create nginx group
  group:
    name: nginx
    system: yes

- name: Create nginx user
  user:
    name: nginx
    group: nginx
    groups: nginx-credentials,public-credentials
    home: /nonexistent
    system: yes

- name: Fix permissions of /var/lib/nginx subdirs
  file:
    path: "/var/lib/nginx/{{ item }}"
    owner: nginx
  with_items:
    - body
    - proxy

- name: Install NGINX systemd unit
  copy:
    src: nginx.service
    dest: /etc/systemd/system/nginx.service
  notify: reload nginx

- name: Install DH parameters
  copy:
    src: "{{ credentials_dir }}/x509/dhparam"
    dest: /etc/nginx/dhparam

- name: Install NGINX config (dirs)
  file:
    path: "/etc/nginx/{{ item.path }}"
    state: directory
  with_filetree: templates/config/
  when: item.state == 'directory'
- name: Install NGINX config (files)
  template:
    src: "{{ item.src }}"
    dest: "/etc/nginx/{{ item.path }}"
  with_filetree: templates/config/
  when: item.state == 'file'
  notify: reload nginx

- name: Create sites-auto directory
  file:
    path: /etc/nginx/sites-auto
    state: directory

# Setup 'default' self-signed cert. The loop is a trick to define the
# 'cn' variable.
- include_role:
    name: public-ssl-cert
  vars:
    cn: default

# Setup per-service endpoints.
- include_tasks: endpoint.yml
  with_subelements:
    - "{{ services.values() }}"
    - public_endpoints
    - { skip_missing: true }

# Create some important locations.
- file: path=/var/www/html/.well-known/acme-challenge state=directory
- template: src=index.html.j2 dest=/var/www/html/index.html

# Create the cache directory.
- file:
    path: /var/cache/nginx
    state: directory
    owner: nginx
    group: nginx
    mode: 0700

# Configure the firewall.
- template:
    src: firewall/20nginx.j2
    dest: /etc/firewall/filter.d/20nginx
  notify: "reload firewall"

