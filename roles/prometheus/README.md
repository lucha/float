prometheus
===

This role sets up monitoring and alerting
using [Prometheus](https://prometheus.io). It will scrape all
*monitoring_targets* defined in your services.yml file, as well as a
number of host-level metrics for every host in the Ansible inventory.

It also includes [Grafana](https://grafana.com) for dashboards.

## Customizing alerts

A few alerting rules are provided by default
in
[rules/prometheus/files/rules/](rules/prometheus/files/rules/). This
includes:

* host-level alerts (high CPU usage, disk full, network errors...)
* service failures (systemd services down, or crash-looping)
* HTTP errors on *public_endpoints*

To add your own alerts, you may want to create your own Ansible role
with the necessary rule and alert files, and schedule it to execute on
hosts in the *prometheus* group. For instance, in your playbook (if
the Ansible role is called *site-alerts*):

```
    - hosts: prometheus
      roles:
        - site-alerts
```

