# Main task for mariadb.

# A single instance is identified by these variables:
# - mariadb_instance (default)
# - mariadb_port (3306)
# - mariadb_metrics_port (9104)
#
# If you are using replication remember to set mariadb_server_id accordingly!
#
# Example usage:
# - include_role:
#     name: mariadb
#   vars:
#     mariadb_instance: apps
#     mariadb_port: 3307
#     mariadb_metrics_port: 9144
#
# The server and client configuration can be found in {{ mariadb_config }}.
# However for convenience a mysql-{{ mariadb_instance }} wrapper is also
# installed.

- file:
    path: "{{ mariadb_config | dirname }}"
    state: directory

- file:
    path: "{{ mariadb_config_dir }}"
    state: directory

- name: Server/Client configuration file
  template:
    src: templates/mariadb.cnf.j2
    dest: "{{ mariadb_config }}"
  register: mariadb_server_config

- name: Install client wrapper
  template:
    src: templates/client_wrapper.j2
    dest: "{{ mariadb_client }}"
    mode: 0555
    owner: root
    group: root

- name: Install server package
  apt:
    name: "{{ item }}"
    state: latest
  with_items:
    - mariadb-client
    - mariadb-server

# Running MariaDB in multi-instance mode with systemd requires some effort:
# the default "mariadb@.service" shipped with the Debian package includes
# Alias directives for "mysql.service" and "mysqld.service", and if we
# understand the problem correctly, systemd will try to enable those aliases
# (by creating those symlinks in /etc/systemd) every time we set up a new
# instance. To avoid that, we ship our own "mariadb@.service" with those
# Alias directives removed.
- name: Mask default services
  systemd:
    masked: yes
    enabled: no
    state: stopped
    name: "{{ item }}"
  with_items:
    - mariadb.service
    - mysql.service
    - mysqld.service
  ignore_errors: yes

- name: Install mariadb multi-instance base systemd unit
  copy:
    src: mariadb@.service
    dest: /etc/systemd/system/mariadb@.service
  register: mariadb_multi_instance_systemd_unit

- name: Install systemd override dir
  file:
    dest: "/etc/systemd/system/{{ mariadb_service }}.d"
    state: directory

- name: Install systemd override file
  template:
    src: templates/service_override.conf.j2
    dest: "/etc/systemd/system/{{ mariadb_service }}.d/role-mariadb.conf"
  register: mariadb_systemd_unit

- name: Bootstrap data directory
  command: "/usr/bin/mysql_install_db --defaults-file={{ mariadb_config }} --datadir={{ mariadb_data_dir }} --user=mysql --auth-root-authentication-method=socket --skip-auth-anonymous-user"
  args:
    creates: "{{ mariadb_data_dir }}"

# Explicitly start the service after bootstrapping data directory
# instead of using a restart handler, so that the database is available
# immediately for subsequent tasks.
- set_fact:
    restart_mariadb: "{{ mariadb_systemd_unit.changed or mariadb_multi_instance_systemd_unit.changed or mariadb_server_config.changed }}"

- name: Start service {{ mariadb_service }}
  systemd:
    name: "{{ mariadb_service }}"
    daemon_reload: "{{ restart_mariadb }}"
    enabled: yes
    state: "{{ 'restarted' if restart_mariadb else 'started' }}"

# Monitoring creates a mysql user, needs the server up.
- import_tasks: monitoring.yml

# Set up replication.
- include_tasks: replication.yml
  when: mariadb_enable_replication
