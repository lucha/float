# Automatically create X509 CSR for services.
#
# This module creates all the required X509 CSR for a given service.
#
# Tasks use this module to then generate certificates on each host.

import os
from ansible.plugins.action import ActionBase


class ActionModule(ActionBase):
    """Automatically create X509 CSRs for a given service."""

    TRANSFERS_FILES = False

    def _get_client_names(self, service_name, domain):
        cn = '%s.%s' % (service_name, domain)
        return cn, [cn, service_name]

    def _get_server_names(self, service_name, domain, hostname):
        # Here we build the list of names (CN and SubjectAltNames) for
        # this certificate. We need to make sure it includes all
        # possible ways in which a client might reach this particular
        # server.
        #
        # The logic in this part of the code should match that used in
        # generating DNS records in servicedns.py.

        # Le the CN be <service name>.<domain>
        cn = '%s.%s' % (service_name, domain)
        alt_names = [cn]

        # Add the short <service name> only.
        alt_names.append(service_name)

        # Services should always be reachable at localhost.
        alt_names.append('localhost')

        # Add the FQDN.
        alt_names.append('%s.%s' % (hostname, domain))

        # Add the short hostname.
        alt_names.append(hostname)

        # TODO: Add the indexed backend name.

        # Add the shard_id name, if present.
        shard_id = self._templar.template('{{shard_id|default("")}}')
        if shard_id:
            alt_names.append('%s.%s.%s' % (shard_id, service_name, domain))

        return cn, alt_names

    def _get_server_ip_addresses(self):
        ip_addrs = ['127.0.0.1', '::1']

        for attr in ('ip', 'ip6'):
            s = self._templar.template('{{%s|default("")}}' % attr)
            if s:
                ip_addrs.append(s)
        for overlay in self._templar.template('{{host_net_overlays}}'):
            s = self._templar.template('{{ip_%s|default("")}}' % overlay)
            if s:
                ip_addrs.append(s)

        return ip_addrs

    def run(self, tmp=None, task_vars=None):
        # Recover important data from Ansible facts.
        hostname = self._templar.template('{{inventory_hostname}}')

        # Retrieve action parameters.
        service_name = self._task.args['service_name']
        domain = self._task.args['domain']
        mode = self._task.args['mode']
        private_key_path = self._task.args['private_key_path']
        cert_path = self._task.args.get('cert_path')
        check_only = self._task.args.get('check', False)
        renew_days = int(self._task.args.get('renew_days', '30'))

        is_client, is_server = False, False
        if mode == 'client':
          cn, alt_names = self._get_client_names(service_name, domain)
          ip_addrs = []
          is_client = True
        elif mode == 'server':
          cn, alt_names = self._get_server_names(service_name, domain, hostname)
          ip_addrs = self._get_server_ip_addresses()
          is_server = True
        else:
          raise Exception('mode must be client or server')

        result = super(ActionModule, self).run(tmp, task_vars)

        changed = True
        subject = 'CN=' + cn
        if check_only:
            changed = self._check_cert(task_vars, subject, alt_names, ip_addrs,
                                       is_client, is_server, renew_days, cert_path)
        else:
            self._make_private_key(task_vars, private_key_path)
            csr = self._make_csr(task_vars, subject, alt_names, ip_addrs, private_key_path)
            result['csr'] = csr

        result['changed'] = changed
        return result

    def _cmd(self, task_vars, args, creates=None):
        args = {
            '_raw_params': ' '.join(args),
            'creates': creates,
        }
        return self._execute_module(
            module_name='command',
            module_args=args,
            task_vars=task_vars,
            wrap_async=False)

    def _make_private_key(self, task_vars, private_key_path):
        cmd = ['x509ca', 'gen-key', '--key=' + private_key_path]
        self._cmd(task_vars, cmd, creates=private_key_path)

    def _make_csr(self, task_vars, subject, alt_names, ip_addrs, private_key_path):
        cmd = ['x509ca', 'csr',
               '--key=' + private_key_path,
               '--subject=' + subject]
        for alt in alt_names:
            cmd.append('--alt=' + alt)
        for ip in ip_addrs:
            cmd.append('--ip=' + ip)
        return self._cmd(task_vars, cmd)['stdout']

    def _check_cert(self, task_vars, subject, alt_names, ip_addrs, is_client,
                    is_server, renew_days, cert_path):
        # Return true if the certificate needs to be re-created.
        cmd = ['x509ca', 'check',
               '--cert=' + cert_path,
               '--subject=' + subject,
               '--renew-days=%d' % renew_days]
        if is_client:
            cmd.append('--client')
        if is_server:
            cmd.append('--server')
        for alt in alt_names:
            cmd.append('--alt=' + alt)
        for ip in ip_addrs:
            cmd.append('--ip=' + ip)

        result = self._cmd(task_vars, cmd)
        return result['rc'] != 0
