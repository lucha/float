# Create DNS entries for service backends.
#
# For now, this simply populates /etc/hosts.
#
# Each service resolves to one or more backends (even though multiple
# IPs aren't really supported in /etc/hosts, we generate them anyway
# waiting for a better solution).
#
# Each host must define at least an 'ip' attribute with its public
# IPv4 address, and optionally an 'ip6' attribute with the public IPv6
# one. The overall network topology is split into 'domains' (the list
# of which includes 'public' by default), and a host might have
# additional IP addresses specific to those domains, specified as
# attributes named 'ip_DOMAIN': for instance if a host belongs to a
# domain named ring0, it should have an attribute 'ip_ring0' with the
# appropriate IP address.
#
# The assumption is that services on hosts within a private network
# domain are exposed on that domain's addresses, so if a host is
# inside that domain it should use those, while hosts outside the
# domain should use the public address instead.
#
# Note that none of this is particularly accurate, currently this is
# just a very rough example of how such routing logic might work.

from ansible.plugins.action import ActionBase


def _host_addrs_by_domain(host_vars):
    # Return a {domain: [addrs]} map of all addresses of this host by
    # domain.
    by_domain = {}
    for key, value in host_vars.iteritems():
        if key == 'ip' or key == 'ip6':
            by_domain.setdefault('public', []).append(value)
        elif key.startswith('ip_'):
            domain = key[3:]
            by_domain.setdefault(domain, []).append(value)
        elif key.startswith('ip6_'):
            domain = key[4:]
            by_domain.setdefault(domain, []).append(value)
    return by_domain


def _make_host_addrs_map(all_hosts, host_vars):
    return dict((host, _host_addrs_by_domain(host_vars.get(host, {})))
                for host in all_hosts)


def _domain_cmp(a, b):
    if a == 'public':
        return 1
    elif b == 'public':
        return -1
    else:
        return cmp(a, b)


def _get_addrs_for(host_addrs_map, src_host, dst_host):
    src_domains = set(host_addrs_map[src_host].keys())
    dst_domains = set(host_addrs_map[dst_host].keys())
    common_domains = src_domains.intersection(dst_domains)
    # Sort the domains in common between src and dst so that 'public'
    # always comes last. This way we will prefer any overlay network
    # (the first in alphabetical order, if the two hosts share more
    # than one) to the public network.
    domain = sorted(common_domains, cmp=_domain_cmp)[0]
    return host_addrs_map[dst_host][domain]


def _service_addrs(hostname, service, service_hosts, host_vars, host_addrs_map):
    dns = {}
    def _add_dns(entry, addrs):
        dns.setdefault(entry, []).extend(addrs)
    for idx, host in enumerate(sorted(service_hosts)):
        addrs = _get_addrs_for(host_addrs_map, hostname, host)

        # <service>.<domain> points at all backends.
        _add_dns(service, addrs)
        # <service>-master.<domain> points at the master for master-electing services.
        if host_vars.get(host, {}).get(service.replace('-', '_') + '_master'):
            _add_dns('%s-master' % service, addrs)
        # <index>.<service>.<domain> points at individual backends.
        _add_dns('%d.%s' % (idx, service), addrs)
        # <shard-id>.<service>.<domain> points at individual backends
        # if they have a 'shard_id' attribute.
        shard_id = host_vars.get(host, {}).get('shard_id')
        if shard_id:
            _add_dns('%s.%s' % (shard_id, service), addrs)
    return dns.items()


def _host_addrs(hostname, host_addrs_map):
    dns = []
    for domain, addrs in host_addrs_map[hostname].iteritems():
        if domain == 'public':
            # <hostname>.<domain> points at the public IP addresses.
            dns.append((hostname, addrs))
        else:
            # <hostname>.<overlay>.<domain> points at the overlay-specific IP addresses.
            dns.append(('%s.%s' % (hostname, domain), addrs))
    return dns


class ActionModule(ActionBase):

    TRANSFERS_FILES = False

    def run(self, tmp=None, task_vars=None):
        hostname = self._templar.template('{{inventory_hostname}}')
        #services = self._templar.template('{{services}}')
        host_vars = self._templar.template('{{hostvars}}')
        assignments = self._templar.template('{{service_assignments}}')
        all_hosts = self._templar.template('{{groups["all"]}}')

        # Create the map of the network, with the addresses of each
        # host split by network domain (public, ring0, etc).
        host_addrs_map = _make_host_addrs_map(all_hosts, host_vars)

        # Accumulate all DNS entries in a list of (name, [addr...])
        # tuples. This is used in the `hosts.j2` template to generate
        # the /etc/hosts file.
        entries = []

        # Host entries go first, so that we get human-readable reverse
        # DNS entries when using /etc/hosts.
        for host in all_hosts:
            entries.extend(_host_addrs(host, host_addrs_map))

        # Service-related entries, from the perspective of the current host.
        for service, service_hosts in assignments.iteritems():
            entries.extend(_service_addrs(
                hostname, service, service_hosts, host_vars, host_addrs_map))

        result = super(ActionModule, self).run(tmp, task_vars)
        result['ansible_facts'] = {'services_dns_map': entries}
        result['changed'] = False
        return result
