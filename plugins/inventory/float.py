from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
    name: float
    plugin_type: inventory
    short_description: FLOAT inventory generator
    description: |

        Reads hosts and service metadata from YAML files, performs
        service assignment, and automatically generates groups and
        variables to enable services.

        * services have been assigned to hosts, by defining variables
          named 'enable_SERVICE' on each host, to allow Ansible roles
          to install or uninstall services

        * a global variable named 'services' holds all the services
          metadata

        * a global variable named 'service_assignments' contains the
          assignments of services to hosts

        Service assignment uses random generators seeded on the
        contents of the configuration itself, so results are
        reproducible between runs and among different users without
        the need to keep explicit state.

        Finally, a global variable named 'default_service_credentials'
        will contain a list of service_credentials that should be
        applied to all hosts (clients for basic infra services).
'''

# Common credentials that are not attached to a service but will be
# installed on all hosts (i.e. clients for basic infra services).
DEFAULT_SERVICE_CREDENTIALS = [
    {'name': 'log-client',
     'enable_server': False},
]

import collections
import os
import random
import yaml
from zlib import crc32

from ansible.errors import AnsibleParserError
from ansible.plugins.inventory import BaseFileInventoryPlugin
from ansible.module_utils._text import to_native

# The basic working schema for this inventory type is:
#
# Point the inventory at a simple YAML file, containing two
# attributes:
#
# * services_file: path to services.yml, contains service metadata
# * hosts_file: path to hosts.yml, contains hosts and scheduling
#   groups
#


def _read_config(loader, cf_path):
    """Read configuration from YAML files.

    Returns a dictionary with 3 items: services, hosts and group_vars.
    """
    try:
        data = loader.load_from_file(cf_path)
    except Exception as e:
        raise AnsibleParserError(
            'Unable to parse %s: %s' % (
                to_native(cf_path), to_native(e)))

    if not data:
        raise AnsibleParserError('%s is empty' % to_native(cf_path))
    elif 'services_file' not in data or 'hosts_file' not in data:
        raise AnsibleParserError(
            '%s is missing required attributes' % to_native(cf_path))

    def _abspath(path):
        # Relative paths in file specs are interpreted as relative to
        # the main configuration file itself.
        if path.startswith('/'):
            return path
        else:
            return os.path.join(os.path.dirname(cf_path), path)

    file_contents = {}
    for tag in ('hosts', 'services'):
        path = _abspath(data[tag + '_file'])
        try:
            tmp = loader.load_from_file(path)
        except Exception as e:
            raise AnsibleParserError(
                'Unable to parse %s_file %s: %s' % (
                    to_native(tag), to_native(path), to_native(e)))
        if not tmp:
            raise AnsibleParserError(
                '%s is empty' % to_native(path))
        elif not isinstance(tmp, dict):
            raise AnsibleParserError(
                '%s has invalid structure, it should be a dictionary,'
                ' got: %s' % (to_native(path), type(data)))
        file_contents[tag] = tmp

    # TODO: It doesn't make much sense to have a default value here,
    # it should probably be removed and replaced with an error.
    credentials_dir = _abspath(data.get('credentials_dir', '../../conf'))
    passwords_file = _abspath(data.get('passwords_file', 'passwords.yml'))
    vars_dir = _abspath(data.get('vars_dir', 'group_vars/all'))
    # Wrap these variables above into group_vars[all].
    group_vars = file_contents['hosts'].get('group_vars', {})
    group_vars.setdefault('all', {}).update({
        'credentials_dir': credentials_dir,
        'vars_dir': vars_dir,
        'passwords_file': passwords_file,
    })

    # Validate contents.
    if not file_contents['services']:
        raise AnsibleParserError('No services defined')
    if not file_contents['hosts'].get('hosts'):
        raise AnsibleParserError('No hosts defined')

    # Split out hosts and group_vars from the contents of the hosts
    # file.
    return {
        'services': file_contents['services'],
        'hosts': file_contents['hosts']['hosts'],
        'group_vars': group_vars,
    }


def _make_services_list(services_dict):
    out = []
    for name, service in services_dict.iteritems():
        service['name'] = name
        out.append(service)
    return out


def _make_groups(hosts):
    groups = {'all': []}
    for name, h in hosts.iteritems():
        groups['all'].append(name)
        for g in h.get('groups', []):
            groups.setdefault(g, []).append(name)
    return groups


def _make_dynamic_groups(hosts):
    # Create groups based on certain host attributes or other dynamic
    # criteria. For now, used to autocreate net-overlay groups, and a
    # generic 'net-overlay' group that includes all hosts with
    # overlays defined.
    dgroups = {}
    overlay_hosts = set()
    for name, h in hosts.iteritems():
        for k, v in h.iteritems():
            # net-overlay group autodetection.
            if k.startswith('ip_'):
                group_name = 'overlay-%s' % k[3:]
                dgroups.setdefault(group_name, []).append(name)
                overlay_hosts.add(name)
    dgroups['net-overlay'] = list(overlay_hosts)
    return dgroups


def _make_dynamic_hostvars(hostvars):
    # Create dynamic per-host variables (based on other host variables
    # defined in the inventory).
    dynamic_vars = {}
    for k, v in hostvars.iteritems():
        if k.startswith('ip_'):
            overlay_name = k[3:]
            dynamic_vars.setdefault('host_net_overlays', []).append(overlay_name)
    return dynamic_vars


def _is_attribute_set(services, attr):
    # Return True if at least one of the services has 'attr' set.
    for s in services.itervalues():
        if s.get(attr):
            return True
    return False


class InventoryModule(BaseFileInventoryPlugin):

    NAME = 'float'

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path, cache=cache)

        conf = _read_config(loader, path)

        services = conf['services']
        services_list = _make_services_list(conf['services'])
        groups = _make_groups(conf['hosts'])
        groups.update(_make_dynamic_groups(conf['hosts']))
        all_hosts = groups['all']

        assignments = self._assign_services(services_list, groups)

        host_vars = {}
        group_vars = conf['group_vars']
        group_vars.setdefault('all', {}).update({
            'services': services,
            'service_assignments': assignments,
            'default_service_credentials': DEFAULT_SERVICE_CREDENTIALS,
            'float_enable_http_frontend': _is_attribute_set(services, 'public_endpoints'),
            'float_enable_tcp_frontend': _is_attribute_set(services, 'public_tcp_endpoints'),
        })

        # Create groups for all services.
        for service in services_list:
            inventory.add_group(service['name'])
        # Initialize host vars.
        for h in all_hosts:
            hv = {
                'enabled_services': [],
                'disabled_services': [],
                'enabled_containers': [],
                'disabled_containers': [],
            }
            hv.update(conf['hosts'][h])
            hv.update(_make_dynamic_hostvars(conf['hosts'][h]))
            host_vars[h] = hv
        # Set variables etc. for assignments.
        for service_name, hosts in assignments.iteritems():
            service_name_var = service_name.replace('-', '_')
            service_tag = 'enable_' + service_name_var
            for h in all_hosts:
                hv = host_vars[h]
                if h in hosts:
                    hv['enabled_services'].append(service_name)
                    hv[service_tag] = 1
                else:
                    hv['disabled_services'].append(service_name)
                    hv[service_tag] = 0
            # Same as above, but for all containers of the service.
            for container in services[service_name].get('containers', []):
                for h in all_hosts:
                    # We need to pass (a reference to) the service
                    # along so it's easier to iterate over the list.
                    data = {'service': services[service_name],
                            'container': container,
                            'tag': '%s-%s' % (service_name, container['name'])}
                    hv = host_vars[h]
                    if h in hosts:
                        hv['enabled_containers'].append(data)
                    else:
                        hv['disabled_containers'].append(data)
            # Run master election if necessary.
            if services[service_name].get('master_election', False):
                candidates = hosts
                if 'master_scheduling_group' in services[service_name]:
                    msg = services[service_name]['master_scheduling_group']
                    candidates = set(groups[msg]).intersection(set(hosts))
                if not candidates:
                    raise Exception(
                        'Could not run master election for service %s: '
                        'none of the hosts "%s" belong to the '
                        'master_scheduling_group %s' % (
                            service_name, hosts, msg))
                master = _master_election(sorted(candidates))
                for h in hosts:
                    host_vars[h][service_name_var + '_master'] = (h == master)

        self._populate_inventory(
            inventory, assignments, host_vars, groups, group_vars)

    def _populate_inventory(self, inventory, assignments, host_vars, groups, group_vars):
        # We have all the data now, let's tell Ansible.
        # Set the host vars.
        for h, hv in host_vars.iteritems():
            inventory.add_host(h)
            for k, v in hv.iteritems():
                inventory.set_variable(h, k, v)
        # Create and populate the hostsfile-driven groups.
        for group_name, hosts in groups.iteritems():
            inventory.add_group(group_name)
            for h in hosts:
                inventory.add_child(group_name, h)
        # Create and populate the per-service dynamic groups.
        for service_name, hosts in assignments.iteritems():
            inventory.add_group(service_name)
            for h in hosts:
                inventory.add_child(service_name, h)
        # Set group vars from the hostfile.
        for g, gv in group_vars.iteritems():
            for k, v in gv.iteritems():
                inventory.set_variable(g, k, v)

    def _assign_services(self, services, groups):
        """Assigns services to hosts.

        Assignments are based on the num_instances, scheduling_group
        (which hosts) attributes, and the current occupation of each
        host.

        By default, services are assigned to the scheduling_group
        'all' with a num_instances of 'all' (so, an instance will be
        running on all known hosts). This is most likely not what you
        want.

        We are using binpack to equally distribute the load of
        services amongst the available hosts out of the scheduling
        group.

        """
        hosts_by_service = {}
        host_occupation = collections.defaultdict(int)
        for service in services:
            available_hosts = sorted(
                groups[service.get('scheduling_group', 'all')])
            num_instances = service.get('num_instances', 'all')
            if num_instances == 'all':
                service_hosts = available_hosts
            else:
                service_hosts = binpack(
                    available_hosts, host_occupation, num_instances)
            hosts_by_service[service['name']] = service_hosts
            for h in service_hosts:
                host_occupation[h] += 1
        return hosts_by_service


def predictable_random(*args):
    """Returns a predictable RNG based on the given args.

    The sequence of generated numbers will be the same every
    invocation, as long as the arguments are identical.
    """
    # Uses crc32(x) as seed.
    return random.Random(crc32(','.join(args)))


def binpack(hosts, occupation_map, n):
    rnd = predictable_random(*hosts)
    result = []
    reverse_occupation_map = {}
    for h in hosts:
        reverse_occupation_map.setdefault(occupation_map[h], []).append(h)
    for v in sorted(reverse_occupation_map.keys()):
        tmp_hosts = reverse_occupation_map[v]
        rnd.shuffle(tmp_hosts)
        result.extend(tmp_hosts)
        if len(result) >= n:
            return result[:n]
    return result


def _master_election(hosts):
    # Predictably select a host among the given list, based on the
    # contents of the list itself.
    rnd = predictable_random(*hosts)
    return rnd.choice(hosts)
