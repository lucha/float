FLOAT
====

*float* is a minimalistic configuration management toolkit to manage
container-based services on bare-metal hardware (a.k.a. *container
orchestration framework*). It is implemented as a series of Ansible
plugins and roles that you should use from your own Ansible
configuration.

Its main purpose is to provide a simple container-oriented
environment, with minimal but complete features, to prepare services
(and developers) for a full migration to something more sophisticated
like Kubernetes.

# Features

Some of these, especially when comparing against full-featured
solutions like Kubernetes, are non-features:

* *static service allocation* - the service scheduler does not move
  containers at runtime in response to host failures, all changes
  happen at "configuration time" when running Ansible.
* *manual port assignments* - you must manually pick a unique port for
  your services, there's no automatic allocation.
* *service discovery protocol* - DNS based.
* *PKI management* - all service-to-service communication can be
  encrypted and authenticated using a private PKI.
* *builtin services* - the toolkit provides a number of built-in
  services, such as monitoring, alerting, log collection and analysis,
  thorough audit functionality, private networking. These services are
  automatically configured and managed (though they can be extended).

# Documentation

More detailed documentation is available in the *docs/* subdirectory,
and in README files for individual Ansible roles:

### General documentation

* [Guide to Ansible integration](docs/ansible.md)
* [Configuration reference](docs/configuration.md)
* [Service discovery protocol](docs/service_mesh.md)
* [HTTP router](docs/http_router.md)
* [Docker usage](roles/docker/README.md)
* [Testing](docs/testing.md)

### Built-in services documentation

* [Monitoring and alerting](roles/prometheus/README.md)
* [Log management and analysis](roles/log-collector/README.md)
* [Authoritative public DNS](roles/dns/README.md)

Built-in services are currently implemented with Ansible roles, and do
not run in containers. But this is just an implementation detail, and
in the future they could be moved to containers without requiring any
changes in the clients.

# Requirements

On the local machine (the one that will run Ansible), you're going to
need [Ansible](https://ansible.com), obviously, and a few small other
custom tools used to manage credentials. These tools should be built
on the local machine using [Go](https://golang.org):

```
    sudo apt-get install golang-go
    go get -u git.autistici.org/ale/x509ca
    go get -u git.autistici.org/ale/ed25519gen
    export PATH=$PATH:$HOME/go/bin
```

Altough not strictly a requirement, you will probably want to use a
number of external services that are not provided by *float* itself:

* git repository hosting
* CI system to build container images
* a Docker registry
