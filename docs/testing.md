Testing
===

This repository contains some integration tests that use Vagrant to
spin up virtual machines and run Ansible against them. The virtual
machines will be destroyed and re-created every time the tests run, so
it would be a good idea to use a local caching proxy for Debian
packages (such as *apt-cacher-ng*).

## Networking

The virtual machines used in tests make use of the 192.168.10.0/24
network. If it is already in use by your local environment, you're
going to have trouble unfortunately. On this network, the local (host)
machine will have the address 192.168.10.1.

## Running tests

To run a test, go to the *test* subdirectory of this repository, and
run the *run-test.sh* command, with the name of the subdirectory
containing the test environment:

```
cd test
./run-test.sh --apt-proxy 192.168.10.1:3142 test-base
```

